# Import os library
import os

# Import flask and template operators
from flask import Flask, send_from_directory, render_template, url_for

# Define the app object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# HTTP 404 error handling
@app.errorhandler(404)
def not_found(error):
    image = url_for('static', filename='404.png')
    return render_template('404.html', image=image), 404

# Serve Favicon.ico
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static/icons'),
                                'favicon.ico', mimetype='image/vnd.microsoft.icon')

# Demo Index
@app.route('/')
def index():
    demo = url_for('static', filename='manifest/stock/stock.mpd')
    webm = url_for('static', filename='manifest/demo/demo.webm')
    return render_template('demo.html', demo=demo, webm=webm), 200