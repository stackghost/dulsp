# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "1d6d2896b3511d8fc10bce8369e372dc7e93d140fb051a033247a242323492fe"

# Secret key for signing cookies
SECRET_KEY = "e6afe6d456af4022160532e487d070c646350307fa821eaed744816d9506e13b"
