#!/usr/bin/env python3
# Run app server
from app import app

# Run Development
#app.run(host='0.0.0.0', port=5000, debug=True)

# Run Production
app.run(host='0.0.0.0', port=80, debug=False)
